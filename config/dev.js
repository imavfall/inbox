const { Sequelize } = require("sequelize");
const sequelize = new Sequelize(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASS, {
  host: process.env.DB_HOST,
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
  },
  define: {
    timestamps: false
  }
});

sequelize.authenticate().then(() => {
  console.log('Connection to dabase successfull');
}).catch((e) => {
  console.log(e)
  process.exit(1)
});
// sequelize.sync();
module.exports = sequelize
