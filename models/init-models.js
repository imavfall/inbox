var DataTypes = require("sequelize").DataTypes;
var _Caracteristic = require("./Caracteristic");
var _Like = require("./Like");
var _Match = require("./Match");
var _Message = require("./Message");
var _Photo = require("./Photo");
var _User = require("./User");
var _inbox = require("./inbox");

function initModels(sequelize) {
  var Caracteristic = _Caracteristic(sequelize, DataTypes);
  var Like = _Like(sequelize, DataTypes);
  var Match = _Match(sequelize, DataTypes);
  var Message = _Message(sequelize, DataTypes);
  var Photo = _Photo(sequelize, DataTypes);
  var User = _User(sequelize, DataTypes);
  var inbox = _inbox(sequelize, DataTypes);

  Caracteristic.belongsTo(User, { foreignKey: "user_id"});
  User.hasOne(Caracteristic, { foreignKey: "user_id"});
  Like.belongsTo(User, { foreignKey: "user_id"});
  User.hasMany(Like, { foreignKey: "user_id"});
  Like.belongsTo(User, { foreignKey: "sender"});
  User.hasMany(Like, { foreignKey: "sender"});
  Match.belongsTo(User, { foreignKey: "first_user_id"});
  User.hasMany(Match, { foreignKey: "first_user_id"});
  Match.belongsTo(User, { foreignKey: "second_user_id"});
  User.hasMany(Match, { foreignKey: "second_user_id"});
  Message.belongsTo(User, { foreignKey: "from"});
  User.hasMany(Message, { foreignKey: "from"});
  Message.belongsTo(User, { foreignKey: "to"});
  User.hasMany(Message, { foreignKey: "to"});
  Message.belongsTo(Match, { foreignKey: "match_id"});
  Match.hasMany(Message, { foreignKey: "match_id"});
  Photo.belongsTo(Caracteristic, { foreignKey: "user_id"});
  Caracteristic.hasOne(Photo, { foreignKey: "user_id"});
  inbox.belongsTo(Match, { foreignKey: "match_id"});
  Match.hasOne(inbox, { foreignKey: "match_id"});

  return {
    Caracteristic,
    Like,
    Match,
    Message,
    Photo,
    User,
    inbox,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
