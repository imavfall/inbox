const { DataTypes } = require("sequelize");
const sequelize = require('../config/dev');
const Match = require('../models/Match.js');
const Message = require('../models/Message.js');
const error_handling = require('../tools/error_handling.js');

const matchInstance = Match(sequelize, DataTypes);
const messageInstance = Message(sequelize, DataTypes);

// matchInstance.hasMany(messageInstance);
exports.list_all_inbox = async function(req, res) {
  try {
    var inbox = await matchInstance.findAll({
      attributes: [[sequelize.fn('DISTINCT', sequelize.col('match_id')) ,'match_id']]
    })
    res.json(inbox);
  } catch (e) {
    error_handling(e, res)
  }
};

exports.list_match_inbox = async function(req, res) {
  try {
    var messages = await messageInstance.findAll(req.body, {
      where: {
        match_id: req.params.match_id
      }
    });
    res.json(messages);
  } catch (e) {
    error_handling(e, res);
  }
};

exports.post_an_inbox = async function(req, res) {
  try {
    var message = await messageInstance.create(req.body);
    res.json(message);
  } catch (e) {
    error_handling(e, res)
  }
};

exports.post_a_message = async function(req, res) {
  try {
    var message = await messageInstance.create(req.body);
    res.json(message);
  } catch (e) {
    error_handling(e, res)
  }
};
